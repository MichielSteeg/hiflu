Template.auth.helpers({
	fbLoggedIn: function() {
		var user = Meteor.user();
		if ( user )
			if ( user.services )
				return user.services.facebook;
		else
			return false;
	}
});

Template.auth.events({
	'click #login': function(event, template) {
		var form = $('#login-form');
		form.submit();
	},
	'click #register': function(event, template) {
		var form = $('#register-form');
		form.submit();
	},
	'click #register': function(event, template) {
		var form = $('#register-form');
		form.submit();
	},
	'click #fb-login': function(event, template) {
		Meteor.loginWithFacebook({}, function(err) {
			if (err) {
				throw new Meteor.Error("Facebook login failed");
			}
		});
	},
	'click #fb-logout': function(event, template) {
		Meteor.logout(function(err) {
			if (err) {
				throw new Meteor.Error("Logout failed");
			}
		});
	},
	'submit #login-form': function(e, t) {
		e.preventDefault();

		var email = t.find('#login-username').value,
			passw = t.find('#login-password').value;

		// Trim and validate
		//
		Meteor.loginWithPassword(email, passw, function(err) {
			if (err) {
				console.log(err);
			} else {
				// User has been logged in
				console.log('logged in');
			}
		});
		return false;
	},
	'submit #register-form': function(e, t) {
		e.preventDefault();

		var uname = t.find('#register-username').value,
			email = t.find('#register-email').value,
			passw = t.find('#register-password').value,
			avatar = '';

		console.log(uname, email, passw, avatar);
		// Trim and validate
		//
		
		user = {
			email: email,
			password: passw,
			profile: {
				name: uname
			}
		};
		
		Accounts.createUser( user, function(err) {
			if (err) {
				//
				console.log(err);
			} else {
				//
			}
		});
		return false;

		Accounts.validateNewUser(function (user) {
			if ( user.username.length < 5 ) {
				throw new Meteor.Error(403, 'Usernames must contain at least 5 characters');
			}

			var passwordTest = new RegExp("(?=.{6,}).*", "g");
			if ( passwordTest.test(user.password) == false) {
				throw new Meteor.Error(403, 'Chosen password is too weak!');
			}

			return true;
		})
	},
	'change .uploadAvatar': function(event, template) {
		//
	},
});