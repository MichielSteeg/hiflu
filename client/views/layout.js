/**
 * Script file for the Layout template
 */

Template.layout.rendered = function() {
	var circles = $('.parallax-viewport .circle');
	var w = window.innerWidth,
		h = window.innerHeight;
	circles.each( function(index, element) {
		var el = $(element);
		var x = getRandomInt(0,w - el.width()),
			y = getRandomInt(0,h - el.height());
		el.css({top: y, left: x});
	});

	$('.parallax-viewport').parallaxify();

	function getRandomInt(min, max) {
	  return Math.floor(Math.random() * (max - min)) + min;
	}
};