/**
 * Script file for Profile view
 */

Template.profile.helpers({
	formatTime: function(time) {
		var time = time.toString().slice(0, -30);
		return time;
	},
	profileOwner: function() {
		var uid = Meteor.userId(),
			id = Template.currentData().profile._id;
		return uid == id;
	},
	profileTags: function() {
		var id = Template.currentData().profile._id;
		var profileTags = ProfileTags.findOne({profileId: id});
		tags = profileTags.tags;
		return tags;
	},
	selectedTag: function(name) {
		var selected = false;

		if ( ! selected ){
			_.each(tags, function(tag) {
				if ( name == tag.tagName ) {
					selected = true;
					return "selected";
				} else
					return null;
			});
		}
		return selected;
	}
});

Template.profile.events({
	'click .btn.reply': function(e, t) {
		var id = Template.currentData().profile._id,
			name = t.find('#reply-form #reply-name').value,
			text = t.find('#reply-form #reply-text').value;
		var post = { id: id,
					 name: name,
					 text: text
				   };
		Meteor.call('addComment', post);

		$('#reply-name, #reply-text').value = '';
	}
});

Template.profile.rendered = function() {
	console.log(Template.currentData());
	var id = Template.currentData().profile._id;
	$('#profileTags').select2({
		width: '240px'
	}).trigger('change').on('change', function(e) {
		var chosen = $('.select2').find('.select2-selection__choice');
		var tags = [];
		chosen.each(function(index, element) {
			var el = $(element);
			var title = el.attr('title');
			var tag = Tags.findOne({name: title});
			var tagId = tag._id;
			var choice = {tagId: tagId, tagName: title};
			tags.push(choice);
		});
		var selection = {profileId: id, tags: tags};
		console.log(selection);
		Meteor.call('setProfileTags',selection);
	});
}