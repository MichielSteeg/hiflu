Meteor.methods({
	'addComment': function(post) {
		var name = post.name,
			text = post.text,
			id = post.id;
		Comments.insert({
			name: name,
			text: text,
			time: new Date(),
			profile: id,
			owner: Meteor.userId()
		});
	},
	deleteComment: function( commentId ) {
		Comments.remove(commentId);
	},
	testComment: function(id, name, text) {
		console.log(id,name,text);
	},
	'setProfileTags': function(post) {
		var pid = post.profileId,
			tags = post.tags;
		if ( ProfileTags.findOne({profileId: pid}) ) {
			var returnId = ProfileTags.update({profileId: pid}, {
				$set: {tags: tags}
			});
		} else {
			var returnId = ProfileTags.insert({profileId: pid, tags: tags});
		}
		console.log(returnId);
	}
});