Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

Router.route('/', {
	name: 'auth',
	waitOn: function() {
		return Meteor.subscribe('images');
	},
	onBeforeAction: function(pause) {
		if( ! Meteor.user() ) {
			this.render('auth');
		} else {
			Router.go('profile');
		}
	}
});
Router.route('/profile/:_id', {
	name: 'userProfile',
	template: 'profile',
	onBeforeAction: function(pause) {
		if ( ! Meteor.user() ) {
			Router.go('auth');
		}
		this.next();
	},
	waitOn: function() {
		return Meteor.subscribe('profileData',this.params._id);
	},
	data: function() {
		templateData = { profile: Meteor.users.findOne({ _id: this.params._id}),
						 comments: Comments.find({profile: this.params._id},{sort: {time: -1}}),
						 profileTags: ProfileTags.find({ profileId: this.params._id}),
						 tags: Tags.find({})
					 	};
		return templateData;
	}
});
Router.route('/profile', {
	name: 'profile',
	template: 'profile',
	onBeforeAction: function(pause) {
		if ( Meteor.user() ) {
			var uid = Meteor.userId();
			Router.go('/profile/'+uid);
		} else {
			Router.go('auth');
		}
		this.next();
	},
	waitOn: function() {
		return Meteor.subscribe('userData');
	}
})
Router.route('/logout', {
	name: 'logout',
	onBeforeAction: function(pause) {
		Meteor.logout();
		if ( ! Meteor.user() )
			Router.go('auth');
		else
			Router.go('logout');
		this.next();
	},
	waitOn: function() {
		return Meteor.subscribe('userData');
	}
})