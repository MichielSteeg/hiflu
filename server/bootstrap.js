// if the database is empty on server start, create some sample data.
Meteor.startup(function () {

  if (Tags.find().count() === 0) {
    var data = [
      'address',
      'applet',
      'area',
      'a',
      'base',
      'basefont',
      'big',
      'blockquote',
      'body',
      'br',
      'b',
      'caption',
      'center',
      'cite',
      'code',
      'dd',
      'div',
      'dl',
      'dt',
      'em',
      'font',
      'form',
      'h1',
      'h2',
      'h3',
      'head',
      'hr',
      'html',
      'img',
      'input',
      'link',
      'li',
      'map',
      'menu',
      'meta',
      'ol',
      'option',
      'param',
      'script',
      'select',
      'table',
      'textarea',
      'var'
    ];

    _.each(data, function(tag) {
      console.log(tag);
      Tags.insert({name: tag});
      // Tags.insert({name: tag.name});
    });
  }
});
