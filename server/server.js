Comments = new Mongo.Collection('comments');
Tags = new Mongo.Collection('tags');
ProfileTags = new Mongo.Collection('profileTags');
Images = new Mongo.Collection('images');

Accounts.onCreateUser( function(options, user) {
	if ( user.services ) {

		if ( options.profile )
			user.profile = options.profile;

		var service = _.keys(user.services)[0];
		var email = user.services[service].email;
		console.log(email);
		if (! email) {
			if (user.emails)
				email = user.emails.address;
		}
		if ( ! email )
			email = options.email;
		if ( ! email ) // Without email, we can't link it up
			return user;

		// if ( ! user.profile.avatar )
		// 	var avatar = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
		// 	user.profile.avatar = avatar;

		var existingUser = Meteor.users.findOne({'emails.address': email});
		if ( ! existingUser )
			return user;

        if (!existingUser.services) {
            existingUser.services = { resume: { loginTokens: [] }};
        }

        existingUser.services[service] = user.services[service];
        existingUser.services.resume.loginTokens.push(
            user.services.resume.loginTokens[0]
        );
        
        Meteor.users.remove({_id: existingUser._id}); // remove existing record
        return existingUser;   
	} 
})