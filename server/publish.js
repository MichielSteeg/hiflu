Meteor.publish('userData', function() {
	var currentUser = this.userId;
	if ( currentUser ) {
		return Meteor.users.find({
			_id: currentUser
		}, {
			fields: {
				"services.facebook.name": 1,
			}
		});
	} else {
		return this.ready();
	}
});

Meteor.publish('profileData', function(userId) {
	return Meteor.users.find({_id: userId});
});

Meteor.publish('comments', function() {
	return Comments.find({},{sort: {time: -1}});
});

Meteor.publish('tags', function() {
	return Tags.find();
});

Meteor.publish('profileTags', function(profileId) {
	return ProfileTags.find();
});

Meteor.publish("images", function(){
  return Images.find();
});