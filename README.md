# README #


### What is this repository for? ###

* Demo app with authentication and profile views

### How do I get set up? ###

* git clone https://MichielSteeg@bitbucket.org/MichielSteeg/hiflu.git dir_name
* cd dir_name
* meteor
* App runs at localhost:3000

### How to install Meteor? ###

* **UNIX:** curl https://install.meteor.com/ | sh
* **WIN:** [Meteor installer](https://install.meteor.com/windows)